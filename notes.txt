MongoDB Atlas?

MongoDB Atlas is a MongoDB database in the cloud. It is the cloud service of MongoDB, the leading NoSQL database.

What is Robo3T?
Robo3T is an application which allows the use of GUI to manipulate MongoDB databases. The advantage of Robo3T is just using atlas is that Robo3T is a local application.

What is Shell?
The shell is an interface in MongoDB that allows us to input commands and perform CRUD on our database.

MongoDb syntax

CRUD Operations:

//Create

	db.collection.insertOne({

		"fieldA": "Value A",
		"fieldB": "Value B"
	
	}) - allows us to insert a document into a collection

	db.collection.insertMany([

		{
			"fieldA": "ValueA",	
			"fieldB": "ValueB",	
		},
		{
			"fieldA": "ValueA",	
			"fieldB": "ValueB",	
		}


	]) - allows ut to insert multiple documents into a collection.

	//Read
	db.collection.find({"criteria": "value"}) - allows us to find or return all documents that matches our criteria.

	db.collection.findOne({"criteria": "value"}) - allows us to find or return the first document that matches our criteria.

	db.collection.find() - allows us to find or return all the documents in a collection.

	db.collection.findOne({}) - allows us to find or return the first item in our collection.

//Update

	db.collection.updateOne({"criteria": "value"}, {$set:{"fieldToBeUpdated":"updatedValue"}}) - allows us to update the first document that matches our criteria.

	db.collection.updateMany({"criteria": "value"},{$set:{"fieldToBeUpdated":"updatedValue"}}) - allows us to update all documents that matches our criteria.

	db.collection.updateOne({},{$set:{"fieldToBeUpdated":"updateValue"}}) - allows us to update the first item in the collection.

	db.collection.updateMany({},{$set:{"fieldToBeUpdate":"updatedValue"}}) - allows us to update all items in the collection.

//Delete

	db.collection.deleteOne({"criteria":"value"}) - allows us to delete the first item that matches our criteria.

	db.collection.deleteMany({"criteria":"value"}) - allows us to delete all items that matches the criteria.

	db.collection.deleteOne({}) - allows us to delete the first item in our collection

	db.collection.deleteMany({}) - allows us to delete all items in the collection.
